import os

import discord
from dotenv import load_dotenv

load_dotenv(dotenv_path="config")

default_intents = discord.Intents.default()
default_intents.members = True
client = discord.Client(intents=default_intents)

@client.event
async def on_ready():
	print("Le robot est prêt.")

@client.event
async def on_message(message):
	if message.content.lower() == "la bibi":
		for i in range(100):
			await message.channel.send("le VRAI SISR", delete_after=5)

@client.event
async def on_member_join(member):
	general_channel: discord.TextChannel = client.get_channel(975921232572719105)
	await general_channel.send(content=f"Que des non SISR {member.display_name} !!! Sous peine d'exclusion.")

@client.event
async def on_message(message):
	if message.content.startswitch("!abracadabra"):

		number = int(message.content.split()[1])
		messages = await message.channel.history(limit=number + 1).flatten()

		for each_message in messages:
			await each_message.delete()

client.run(os.getenv("TOKEN"))